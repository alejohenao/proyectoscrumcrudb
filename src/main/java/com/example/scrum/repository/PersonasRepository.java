package com.example.scrum.repository;

import com.example.scrum.entidades.Personas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonasRepository extends JpaRepository<Personas, Integer>  {
}
