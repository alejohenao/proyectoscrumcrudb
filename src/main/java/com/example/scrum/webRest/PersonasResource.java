package com.example.scrum.webRest;

import com.example.scrum.entidades.Personas;
import com.example.scrum.service.PersonasService;
import com.example.scrum.service.dto.PersonasDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/personas")
public class PersonasResource {

    @Autowired
    PersonasService personasService;
//listar todos
    @GetMapping(" ")
    public ResponseEntity<List<PersonasDto>> findAll(){
        return  new ResponseEntity<>(personasService.findAll(), HttpStatus.OK);
    }

    //listar por id
    @GetMapping("/{id}")
    public ResponseEntity<PersonasDto> findById(@PathVariable Integer id){
        return new ResponseEntity<>(personasService.findById(id),HttpStatus.OK);

    }
// crear
    @PostMapping("")
    public PersonasDto create(@RequestBody PersonasDto personasDto) {
        return personasService.create(personasDto);
    }

 //actualizar

    @PutMapping("")
    public PersonasDto update(@RequestBody  PersonasDto personasDto) {return personasService.update(personasDto);
    }
// eliminar
    @DeleteMapping("/{id}")
    public void delete(@RequestBody @PathVariable Integer id) {personasService.delete(id);
    }
}
