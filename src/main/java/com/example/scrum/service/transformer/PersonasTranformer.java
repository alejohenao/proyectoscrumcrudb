package com.example.scrum.service.transformer;

import com.example.scrum.entidades.Personas;
import com.example.scrum.service.dto.PersonasDto;

public class PersonasTranformer {
    public static Personas getPersonasByPersonasDto(PersonasDto dto){
        if (dto ==null){
            return null;
        }
        Personas personas = new Personas();
        personas.setId(dto.getId());
        personas.setDocumento(dto.getDocumento());
        personas.setNombre(dto.getNombre());
        personas.setApellido(dto.getApellido());
        personas.setDireccion(dto.getDireccion());
        personas.setTelefono(dto.getTelefono());
        return personas;
    }
    public static PersonasDto getPersonasDtoByPersonas(Personas personas){
        if (personas ==null){
            return null;
        }
        PersonasDto dto = new PersonasDto();
        dto.setId(personas.getId());
        dto.setDocumento(personas.getDocumento());
        dto.setNombre(personas.getNombre());
        dto.setApellido(personas.getApellido());
        dto.setDireccion(personas.getDireccion());
        dto.setTelefono(personas.getTelefono());
        return dto;
    }
}
