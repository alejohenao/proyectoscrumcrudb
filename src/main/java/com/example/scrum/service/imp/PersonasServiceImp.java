package com.example.scrum.service.imp;

import com.example.scrum.entidades.Personas;
import com.example.scrum.repository.PersonasRepository;
import com.example.scrum.service.PersonasService;
import com.example.scrum.service.dto.PersonasDto;
import com.example.scrum.service.transformer.PersonasTranformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonasServiceImp implements PersonasService {
    @Autowired
    PersonasRepository personasRepository;
    //listar todos
    @Override
    public List<PersonasDto> findAll(){
        return personasRepository.findAll().stream().map(PersonasTranformer::getPersonasDtoByPersonas)
                .collect(Collectors.toList());
    }
    //listar por id
    @Override
    public PersonasDto findById(Integer id) {
        Optional<Personas> optional = personasRepository.findById(id);
        if (optional.isPresent()){
            return PersonasTranformer.getPersonasDtoByPersonas(optional.get());
        }
       return null;
    }

    //crear
    @Override
    public PersonasDto create(PersonasDto personasDto){
        Personas personas = PersonasTranformer.getPersonasByPersonasDto(personasDto);
        return PersonasTranformer.getPersonasDtoByPersonas(personasRepository.save(personas));
    }
    //actualizar
    @Override
    public PersonasDto update(PersonasDto personasDto){
        Personas personas = PersonasTranformer.getPersonasByPersonasDto(personasDto);
        return PersonasTranformer.getPersonasDtoByPersonas(personasRepository.save(personas));
    }
    //eliminar
    @Override
    public void delete(Integer id){
        personasRepository.deleteById(id);
    }
}
