package com.example.scrum.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@NoArgsConstructor
@Setter
@Getter
public class PersonasDto implements Serializable {
    @Id
    private int id;
    private String documento;
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono;
}
