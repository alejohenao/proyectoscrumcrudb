package com.example.scrum.service;

import com.example.scrum.service.dto.PersonasDto;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface PersonasService {
   List<PersonasDto> findAll();
   PersonasDto findById(Integer id);
   PersonasDto create (PersonasDto personasDto);
   PersonasDto update(PersonasDto personasDto);
   public void delete(Integer id);
}
